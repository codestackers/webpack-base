'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; };

exports.mapOctoberThemes = mapOctoberThemes;
exports.makeConfig = makeConfig;

const fs = require('fs');
const webpack = require('webpack');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');
const HardSourceWebpackPlugin = require('hard-source-webpack-plugin');

/**
 * Map octoberCMS themes
 * 
 * @param {Function} make_config 
 */
function mapOctoberThemes(make_config, root_dir) {
    var root = root_dir || process.cwd();
    var themes = fs.readdirSync(root + '/themes', {
        withFileTypes: true
    });

    return themes.filter(function (dirent) {
        return dirent.isDirectory() && fs.existsSync(root + '/themes/' + dirent.name + '/assets/src/js/index.js');
    }).map(function (dirent) {
        return make_config(root + '/themes/' + dirent.name + '/assets');
    });
}

/**
 * Create webpack base configuration
 * 
 * @param {String} base_path 
 */
function makeConfig(base_path, user_opts) {
    var opts = _extends({}, {
        mode: 'development',
        jquery: false,
        react: false,
        bootstrap: false,
        sass: false
    }, user_opts);

    var provide_config = {};

    if (opts.jquery) {
        provide_config = {
            $: 'jquery',
            jQuery: 'jquery',
            'window.jQuery': 'jquery'
        };
    }

    if (opts.bootstrap) {
        provide_config.Util = 'bootstrap/js/dist/util';
    }

    var config = {
        entry: [base_path + '/src/js/index.js'],
        mode: opts.mode,
        output: {
            path: base_path + '/dist/',
            filename: '[name].bundle.js'
        },
        module: {
            rules: [{
                test: /\.css$/,
                loaders: ['style-loader', 'css-loader', 'resolve-url-loader']
            }, {
                test: /\.(woff(2)?|ttf|eot|svg)(\?v=\d+\.\d+\.\d+)?$/,
                use: [{
                    loader: 'file-loader',
                    options: {
                        useRelativePath: false,
                        name: '[path]/[name].[ext]',
                        publicPath: base_path + '/dist/'
                    }
                }]
            }, {
                test: /\.js[x]?$/,
                exclude: /(node_modules|bower_components)/,
                use: {
                    loader: 'babel-loader?cacheDirectory',
                    options: {
                        presets: opts.react ? ['@babel/preset-env', ['@babel/preset-react', { pragma: 'h' }]] : ['@babel/preset-env'],
                        plugins: opts.react ? [['babel-plugin-transform-react-jsx', { pragma: 'h' }]] : []
                    }
                }
            }]
        },
        optimization: {
            splitChunks: {
                chunks: 'all',
                cacheGroups: {
                    styles: {
                        name: 'styles',
                        test: /\.css$/,
                        chunks: 'all',
                        enforce: true
                    }
                }
            }
        },
        plugins: [new HardSourceWebpackPlugin(), new webpack.ProvidePlugin(provide_config), new MiniCssExtractPlugin({
            filename: '[name].css',
            chunkFilename: '[name].css'
        })]
    };

    if (opts.sass) {
        config.module.rules.push({
            test: /\.scss$/,
            use: ['style-loader', MiniCssExtractPlugin.loader, 'css-loader', 'resolve-url-loader', 'sass-loader']
        });
    }

    return config;
}