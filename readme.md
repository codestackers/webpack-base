## Webpack base configuration
This configuration should be in the root of the OctoberCMS project and will use the themes/* to recursively look at webpack scripts. It will compile the assets for that theme in their own assets/src/dist directory.

# usage
Make sure to have a themes folder, in which a folder exists with the name of your theme. 

- root/themes/*

Next, create an assets folder in the * directory, alongside a src folder.

- root/themes/*/assets/src/

Inside the assets folder, also create a folder named dist. Every file webpack compiles will be put here. 

- root/themes/*/assets/dist

Also make sure to create a js and scss folder. The js folder must include an index.js file, and the scss folder must include an app.scss base file.

- root/themes/*/assets/src/js/index.js
- root/themes/*/assets/src/scss/app.scss

Install webpack in root and run webpack -w
